'use strict';

const Transport = require('azure-iot-device-mqtt').Mqtt;
const Client = require('azure-iot-device').ModuleClient;
const Message = require('azure-iot-device').Message;

const temperatureThreshold = 25;

Client.fromEnvironment(Transport, function (err, client) {
  if (err) {
    throw err;
  } else {
    client.on('error', function (err) {
      throw err;
    });

    // connect to the Edge instance
    client.open(function (err) {
      if (err) {
        throw err;
      } else {
        console.log('IoT Hub module client initialized');

        // Act on input messages to the module.
        client.on('inputMessage', function (inputName, msg) {
          // pipeMessage(client, inputName, msg);
          filterMessage(client, inputName, msg);
        });

        client.getTwin(function (err, twin) {
          if (err) {
              console.error('Error getting twin: ' + err.message);
          } else {
              twin.on('properties.desired', function(delta) {
                  if (delta.TemperatureThreshold) {
                      temperatureThreshold = delta.TemperatureThreshold;
                  }
              });
            }
        });
      }
    });
  }
});

// This function just pipes the messages without any change.
// function pipeMessage(client, inputName, msg) {
//   client.complete(msg, printResultFor('Receiving message'));

//   if (inputName === 'input1') {
//     const message = msg.getBytes().toString('utf8');
//     if (message) {
//       const outputMsg = new Message(message);
//       client.sendOutputEvent('output1', outputMsg, printResultFor('Sending received message'));
//     }
//   }
// }

// This function filters out messages that report temperatures below the temperature threshold.
// It also adds the MessageType property to the message with the value set to Alert.
function filterMessage(client, inputName, msg) {
  client.complete(msg, printResultFor('Receiving message'));
  if (inputName === 'input1') {
      var message = msg.getBytes().toString('utf8');
      var messageBody = JSON.parse(message);
      if (messageBody && messageBody.machine && messageBody.machine.temperature && messageBody.machine.temperature > temperatureThreshold) {
          console.log(`Machine temperature ${messageBody.machine.temperature} exceeds threshold ${temperatureThreshold}`);
          var outputMsg = new Message(message);
          outputMsg.properties.add('MessageType', 'Alert');
          client.sendOutputEvent('output1', outputMsg, printResultFor('Sending received message'));
      }
  }
}

// Helper function to print results in the console
function printResultFor(op) {
  return function printResult(err, res) {
    if (err) {
      console.log(op + ' error: ' + err.toString());
    }
    if (res) {
      console.log(op + ' status: ' + res.constructor.name);
    }
  };
}
